# Setup telegraf for centos 7
# telegraf v1.9.2

# Setup variables
export INFLUX_URL="http:\/\/pc-173.calit2.optiputer.net:8086"
export INFLUX_USER="kenadmin"
export INFLUX_PASSWORD="kenpassword"
export TELEGRAF_INTERVAL="5s"
export TELEGRAF_INPUTS="cpu:mem:net:swap:disk:diskio:netstat:system"
# export TELEGRAF_HOSTNAME="test-instance-1" # uncomment if you want to use same hostname

# Add telegraf repo see https://docs.influxdata.com/telegraf/v1.9/introduction/installation/
cat <<EOF | sudo tee /etc/yum.repos.d/influxdb.repo
[influxdb]
name = InfluxDB Repository - RHEL \$releasever
baseurl = https://repos.influxdata.com/rhel/\$releasever/\$basearch/stable
enabled = 1
gpgcheck = 1
gpgkey = https://repos.influxdata.com/influxdb.key
EOF

# Update and install telegraf and start service
sudo apt-get install -y telegraf

# Backup a telegraf original config file
sudo cp /etc/telegraf/telegraf.conf /etc/telegraf/telegraf.conf.bak

# Generate telegraf inputs script and save to file
# see more: https://docs.influxdata.com/telegraf/v1.9/plugins/inputs/
#           https://github.com/influxdata/telegraf/tree/master/plugins/inputs

# Generate telegraf inputs script and save to file
sudo sh -c "telegraf --input-filter $TELEGRAF_INPUTS --output-filter influxdb config | tee > /etc/telegraf/telegraf.conf"

# Edit configuration file
sudo sed -i "s/  # urls = \[\"http:\/\/127.0.0.1:8086\"\]/  urls = \[\"${INFLUX_URL}\"\]/" /etc/telegraf/telegraf.conf
sudo sed -i "s/  # username = \"telegraf\"/  username = \"$INFLUX_USER\"/" /etc/telegraf/telegraf.conf
sudo sed -i "s/  # password = \"metricsmetricsmetricsmetrics\"/  password = \"$INFLUX_PASSWORD\"/" /etc/telegraf/telegraf.conf
sudo sed -i "s/  interval = \"10s\"/  interval = \""$TELEGRAF_INTERVAL\""/" /etc/telegraf/telegraf.conf
sudo sed -i "s/  hostname = \"\"/  hostname = \""$TELEGRAF_HOSTNAME"\"/" /etc/telegraf/telegraf.conf

# Start telegraf service
sudo systemctl restart telegraf
