### Install telegraf on CentOS7 using  

```bash
sudo sh -c 'curl -o- https://gitlab.com/Senior-Project-Playground/centos-scripts-sandbox/raw/master/scripts/telegraf/install-telegraf.sh | bash'
```

### Uninstall telegraf on CentOS7 using

```bash
sudo sh -c 'curl -o- https://gitlab.com/Senior-Project-Playground/centos-scripts-sandbox/raw/master/scripts/telegraf/uninstall-telegraf.sh | bash'
```
