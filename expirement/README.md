## Sar with timeout

To use sar with timeout by `sar <every n seconds> <n times>`. For example collecting status of cpu every 1 second for 5 times

```bash
sar 1 5
```

---

# Collecting system information using `sar`

## CPU Metrics

```bash
[ayuth_mang@centos-instance ~]$ sar 1 3
Linux 3.10.0-957.1.3.el7.x86_64 (centos-instance)       02/21/2019      _x86_64_        (1 CPU)

06:35:31 AM     CPU     %user     %nice   %system   %iowait    %steal     %idle
06:35:32 AM     all      0.00      0.00      0.00      0.00      0.00    100.00
06:35:33 AM     all      0.00      0.00      0.00      0.00      0.00    100.00
06:35:34 AM     all      0.00      0.00      0.99      0.00      0.00     99.01
Average:        all      0.00      0.00      0.33      0.00      0.00     99.67
```

## Memory Metrics

```bash
[ayuth_mang@centos-instance ~]$ sar -r 1 3
Linux 3.10.0-957.1.3.el7.x86_64 (centos-instance)       02/21/2019      _x86_64_        (1 CPU)

06:36:12 AM kbmemfree kbmemused  %memused kbbuffers  kbcached  kbcommit   %commit  kbactive   kbinact   kbdirty
06:36:13 AM    205976    395352     65.75         0    191620    471220     78.36    209608     92648         0
06:36:14 AM    205976    395352     65.75         0    191620    471220     78.36    209616     92648         0
06:36:15 AM    205580    395748     65.81         0    191620    471188     78.36    209620     92648         0
Average:       205844    395484     65.77         0    191620    471209     78.36    209615     92648         0
```

## Disk Metrics

```bash
[ayuth_mang@centos-instance ~]$ sar -d 1 3
Linux 3.10.0-957.1.3.el7.x86_64 (centos-instance)       02/21/2019      _x86_64_        (1 CPU)

06:36:28 AM       DEV       tps  rd_sec/s  wr_sec/s  avgrq-sz  avgqu-sz     await     svctm     %util
06:36:29 AM    dev8-0      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00

06:36:29 AM       DEV       tps  rd_sec/s  wr_sec/s  avgrq-sz  avgqu-sz     await     svctm     %util
06:36:30 AM    dev8-0      1.00      0.00      8.00      8.00      0.02     21.00     21.00      2.10

06:36:30 AM       DEV       tps  rd_sec/s  wr_sec/s  avgrq-sz  avgqu-sz     await     svctm     %util
06:36:31 AM    dev8-0      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00

Average:          DEV       tps  rd_sec/s  wr_sec/s  avgrq-sz  avgqu-sz     await     svctm     %util
Average:       dev8-0      0.33      0.00      2.67      8.00      0.01     21.00     21.00      0.70
```

## Network Metrics

```bash
[ayuth_mang@centos-instance ~]$ sar -n DEV 1 3
Linux 3.10.0-957.1.3.el7.x86_64 (centos-instance)       02/21/2019      _x86_64_        (1 CPU)
06:37:02 AM     IFACE   rxpck/s   txpck/s    rxkB/s    txkB/s   rxcmp/s   txcmp/s  rxmcst/s
06:37:03 AM      eth0      1.98      0.99      0.13      0.13      0.00      0.00      0.00
06:37:03 AM        lo      0.00      0.00      0.00      0.00      0.00      0.00      0.00
06:37:03 AM     IFACE   rxpck/s   txpck/s    rxkB/s    txkB/s   rxcmp/s   txcmp/s  rxmcst/s
06:37:04 AM      eth0      1.00      1.00      0.06      0.21      0.00      0.00      0.00
06:37:04 AM        lo      0.00      0.00      0.00      0.00      0.00      0.00      0.00
06:37:04 AM     IFACE   rxpck/s   txpck/s    rxkB/s    txkB/s   rxcmp/s   txcmp/s  rxmcst/s
06:37:05 AM      eth0      1.00      1.00      0.06      0.39      0.00      0.00      0.00
06:37:05 AM        lo      0.00      0.00      0.00      0.00      0.00      0.00      0.00
Average:        IFACE   rxpck/s   txpck/s    rxkB/s    txkB/s   rxcmp/s   txcmp/s  rxmcst/s
Average:         eth0      1.33      1.00      0.09      0.24      0.00      0.00      0.00
Average:           lo      0.00      0.00      0.00      0.00      0.00      0.00      0.00
```
